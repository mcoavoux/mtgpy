if [ "$#" -le 2 ]
then
    echo "illegal number of parameters"
    echo Usage:
    echo    "bash expe_main.sh (negra|dptb|tiger_spmrl) modelname *args"
    exit 1
fi

source /home/getalp/coavouxm/anaconda3/bin/activate base
mod=$2
mkdir -p ${mod}

git rev-parse HEAD > ${mod}/git_rev
conda list --explicit > ${mod}/conda-requirements.txt
pip freeze > ${mod}/requirements.txt

corpus=$1

tr=~/data/multilingual_disco_data/data/${corpus}/train.discbracket
dev=~/data/multilingual_disco_data/data/${corpus}/dev.discbracket

dtok=~/data/multilingual_disco_data/data/${corpus}/dev.tokens
ttok=~/data/multilingual_disco_data/data/${corpus}/test.tokens

tgold=~/data/multilingual_disco_data/data/${corpus}/test.discbracket


shift
shift


args=" --train ${tr} --dev ${dev} --model ${mod} $@"

cd ../src2
echo python mtg.py train ${args}                      > ${mod}/commandline

python mtg.py train ${args}                      > ${mod}/log.txt 2> ${mod}/err.txt &&
python mtg.py eval  ${args} ${dtok} ${mod}/dev_pred.discbracket  --gold ${dev}   > ${mod}/eval_dev &&
python mtg.py eval  ${args} ${ttok} ${mod}/test_pred.discbracket --gold ${tgold} > ${mod}/eval_test

