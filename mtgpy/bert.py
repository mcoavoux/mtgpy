import logging
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.utils.rnn import pad_sequence
from transformers import BertTokenizer, BertModel
from transformers import AutoTokenizer, AutoModel

class BertEncoder(nn.Module):

    def __init__(self, bert_id, layer):
        """
        bert_id: 
        layer: index of layer to use for token features
        """
        super(BertEncoder, self).__init__()
        self.tokenizer = AutoTokenizer.from_pretrained(bert_id, do_lower_case=("uncased" in bert_id))
        self.bert = AutoModel.from_pretrained(bert_id)
        
        self.layer = layer

        self.replace = {"-LRB-": "(", "-RRB-": ")", "#LRB#": "(", "#RRB#": ")"}

        self.dim = self.bert.config.hidden_size
        
        self.cache_tokenizer = {}

    def replace_parentheses(self, token_list):
        return [self.replace[token] if token in self.replace else token for token in token_list]

    def forward(self, sentence, batch):
        
        if not batch:
            return self.forward([sentence], batch=True)
        batch_tokens = []
        batch_masks = []
        batch_lengths = []
        for token_list in sentence:
            token_list = self.replace_parentheses(token_list)

            wptokens = []
            mask = []
            for token in token_list:
                if token in self.cache_tokenizer:
                    wp = self.cache_tokenizer[token]
                else:
                    wp = self.tokenizer.tokenize(token)
                    self.cache_tokenizer[token] = wp

                wptokens.extend(wp)
                mask.append(1)
                for i in range(len(wp)-1):
                    mask.append(0)

            #mask = [0 if tok[:2] == "##" else 1 for tok in wptokens]
            mask = torch.tensor(mask, dtype=torch.bool, device=self.bert.device)

            indexed_tokens = self.tokenizer.convert_tokens_to_ids(wptokens)
            tokens_tensor = torch.tensor([indexed_tokens], device = self.bert.device)

            batch_tokens.append(tokens_tensor.view(-1))
            batch_masks.append(mask)
            batch_lengths.append(len(wptokens))
        
        padded = pad_sequence(batch_tokens, batch_first=True)
        mask = (padded != 0).long()

        #encoded_layers = self.bert(input_ids=padded, attention_mask=mask)[0]
        bert_output = self.bert(input_ids=padded, attention_mask=mask, output_hidden_states=True)
        
        # TODO: agreggate over layers?
        if self.layer is None:
            encoded_layers = bert_output["last_hidden_state"]
            
            print("a", encoded_layers.shape)
        else:
            encoded_layers = bert_output["hidden_states"][-self.layer:]
            encoded_layers = torch.mean(torch.stack(encoded_layers),dim=0)  # average of n last layers

        split_layers = encoded_layers.split([1 for _ in sentence])
        assert(len(split_layers) == len(batch_masks))
        filtered_layers = [layer.squeeze(0)[:l][m] for layer, l, m in zip(split_layers, batch_lengths, batch_masks)]

        return filtered_layers



if __name__ == "__main__":

    for bert_id in ['bert-base-cased',"flaubert/flaubert_base_cased", "/home/coavouxm/data/panta/Text_Base_fr_4GB_v0/HuggingFace"]:
        bert = BertEncoder(bert_id, layer=4)
        bert.eval()

        #bert.cuda()

        sentence = "The bill , whose backers include Chairman Dan Rostenkowski -LRB- D. , Ill. -RRB- , would prevent the Resolution Trust Corp. from raising temporary working capital by having an RTC-owned bank or thrift issue debt that would n't be counted on the federal budget .".split()
        #sentence = ["the cat eats an apple".split(), "the cat is antiantipolitical".split()]

        print(len(sentence))

        output = bert([sentence[:10], sentence], batch=True)
        print(type(output))
        print(output[0].shape)
        print(output[1].shape)
        
        print(output[1])
        output2 = bert(sentence, batch=False)
        
        print(output2[0])

