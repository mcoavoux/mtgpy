import torch.nn as nn
import torch.nn.init
import torch.nn.functional as F
from torch.nn import LayerNorm

class SentenceEncoderLSTM(nn.Module):
    def __init__(self, args, d_input):
        super(SentenceEncoderLSTM, self).__init__()

        self.args = args

        self.word_transducer_l1 = nn.LSTM(input_size=d_input,
                                       hidden_size=args.lstm_dim //2,
                                       num_layers=1,
                                       dropout=args.dropout_lstm,
                                       batch_first=True,
                                       bidirectional=True)

        assert(args.lstm_layers >= 2)
        self.word_transducer_l2 = nn.LSTM(input_size=args.lstm_dim,
                                       hidden_size=args.lstm_dim //2,
                                       num_layers=args.lstm_layers -1,
                                       dropout=args.dropout_lstm,
                                       batch_first=True,
                                       bidirectional=True)


        self.residual = args.lstm_residual_connection

        #self.layer_norm_l0 = nn.LayerNorm(d_input)
        #self.layer_norm_l1 = nn.LayerNorm(args.lstm_dim)
        self.layer_norm_l2 = nn.LayerNorm(args.lstm_dim)


    def forward(self, all_embeddings, lengths):

        packed_padded_char_based_embeddings = torch.nn.utils.rnn.pack_padded_sequence(
                                all_embeddings, lengths, batch_first=True)

        output_l1, (h_n, c_n) = self.word_transducer_l1(packed_padded_char_based_embeddings)
        output_l2, (h_n, c_n) = self.word_transducer_l2(output_l1)

        unpacked_l1, _ = torch.nn.utils.rnn.pad_packed_sequence(output_l1, batch_first=True)
        unpacked_l2, _ = torch.nn.utils.rnn.pad_packed_sequence(output_l2, batch_first=True)

        if self.residual:
            unpacked_l2 = unpacked_l2 + unpacked_l1

        unpacked_l1 = [t.squeeze(0) for t in unpacked_l1.split([1 for l in lengths], dim=0)]
        unpacked_l1 = [t[1:l-1,:] for t, l in zip(unpacked_l1, lengths)]


        unpacked_l2 = [t.squeeze(0) for t in unpacked_l2.split([1 for l in lengths], dim=0)]
        unpacked_l2 = [t[:l,:] for t, l in zip(unpacked_l2, lengths)]

        unpacked_l2 = [self.layer_norm_l2(l2) for l2 in unpacked_l2]

        return unpacked_l1, unpacked_l2


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    TransformerNetwork.add_cmd_options(parser)

    args = parser.parse_args()
    
    d_input = 1024
    model = TransformerNetwork(args, d_input)

    args.n_heads = 2
    args.n_layers = 2

    input = torch.rand(3, 10, d_input)

    output, output =  model(input, lengths = torch.tensor([4, 8, 10]))

    print([o.shape for o in output])
