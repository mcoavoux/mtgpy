from mtgpy.tree import Token, Tree, get_yield

ID,FORM,LEMMA,CPOS,FPOS,MORPH,HEAD,DEPREL,PHEAD,PDEPREL=range(10)

def get_conll(tree):

    tokens = get_yield(tree)

    conll_tokens = []
    for tok in tokens :
        newtok = ["_" for i in range(10)]
        newtok[ID]   = str(tok.i)
        newtok[FORM] = tok.token
        newtok[CPOS] = newtok[FPOS] = tok.features[0]
        #newtok[MORPH] = "|".join(sorted(["{}={}".format(a,v) for a,v in zip( Token.header, tok.features[1:] ) if v != "UNDEF"]))
        morph = [f for f in tok.features[1:] if "=_" not in f]
        newtok[MORPH] = "|".join(morph)
        conll_tokens.append(newtok)
    return conll_tokens

def write_conll(ctree, out):
    conll_tokens = get_conll(ctree)
    for tok in conll_tokens:
        out.write("{}\n".format("\t".join(tok)))

def write_conll_corpus(trees, filename):
    with open(filename, "w", encoding="utf8") as f:
        for t in trees:
            write_conll(t, f)
            f.write("\n")

def nltk_tree_to_Tree(nltk_tree):
    # Leaf
    if len(nltk_tree) == 1 and type(nltk_tree[0]) == str:
        idx, token = nltk_tree[0].split("=", 1)
        idx = int(idx)
        return Token(token, idx, [nltk_tree.label()])
    else:
        children = [nltk_tree_to_Tree(child) for child in nltk_tree]
        return Tree(nltk_tree.label(), children)

def read_discbracket_corpus(filename):
    from nltk import Tree as nTree
    with open(filename, encoding="utf8") as f:
        ctrees = [nTree.fromstring(line.strip()) for line in f]
    return [nltk_tree_to_Tree(t) for t in ctrees]

def get_leaves_rec(nltk_tree, leaves):
    if len(nltk_tree) == 1 and type(nltk_tree[0]) == str:
        leaves.append(nltk_tree)
    else:
        for child in nltk_tree:
            get_leaves_rec(child, leaves)

def update_leaves(nltk_tree):
    # Add i=token for each token (convert to discbracket format
    leaves = []
    get_leaves_rec(nltk_tree, leaves)
    for i, leaf in enumerate(leaves):
        leaf[0] = f"{i}={leaf[0]}"

def read_bracket_corpus(filename):
    from nltk import Tree as nTree
    with open(filename, encoding="utf8") as f:
        ctrees = [nTree.fromstring(line.strip()) for line in f]
    for t in ctrees:
        update_leaves(t)
    return [nltk_tree_to_Tree(t) for t in ctrees]

def read_tags(filename):
    output = []
    with open(filename, encoding="utf8") as f:
        header = f.readline().strip().split("\t")
        header = header[1:]
        
        corpus = f.read().split("\n\n")
        for sentence in corpus:
            if not sentence.strip():
                break
            sent_tags = [[], []]
            sentence = sentence.split("\n")
            for line in sentence:
                token, *tags = line.strip().split("\t")
                tags = [tag.split("=") for tag in tags]
                assert(all([tags[i][0] == header[i] for i in range(len(tags))]))
                sent_tags[0].append(token)
                sent_tags[1].append(dict(zip(header,[tag[-1] for tag in tags])))
            output.append(sent_tags)
    return header, output


if __name__ == "__main__":
    import sys

    treebank_2 = read_discbracket_corpus("/home/getalp/coavouxm/data/treebanks/data/dptb/dev.discbracket")
    treebank_3 = read_bracket_corpus("/home/getalp/coavouxm/data/treebanks/data/spmrl_FRENCH/dev.bracket")
    for t in treebank_3[:3]:
        print(t)


    header, tags = read_tags("/home/getalp/coavouxm/data/treebanks/data/spmrl_FRENCH/dev.tags")
    print(header)
    print(tags[0])
    print(tags[-1])
