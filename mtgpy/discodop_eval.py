
import subprocess
import pathlib

def call_eval(gold_file, pred_file, eval_param, disconly=False):
    """Just calls discodop eval and returns Prec, Recall, Fscore as floats"""

    folder_path = pathlib.Path(__file__).parent.resolve()
    
    #params = ["discodop", "eval", gold_file, pred_file, "proper.prm", "--goldfmt=bracket", "--parsesfmt=discbracket"]
    params = ["discodop", "eval", gold_file, pred_file, f"{folder_path}/{eval_param}", "--goldfmt=discbracket", "--parsesfmt=discbracket"]
    if disconly:
        params.append("--disconly")

    result = subprocess.check_output(params)
    result = str(result).split("Summary")
    summary = result[-1]
    summary = summary.split("\\n")[1:]
    # ~ for line in summary:
        # ~ print("l ", line)
    
    output_dict = {}
    for line in summary:
        line = line.split(":")
        if len(line) == 2:
            value = line[1].strip().split()[-1]
            output_dict[line[0].strip()] = value
    
    recall = output_dict["labeled recall"]
    prec   = output_dict["labeled precision"]
    fscore = output_dict["labeled f-measure"]

    if "nan" not in [prec, recall, fscore]:
        return float(prec), float(recall), float(fscore)
    return 0, 0, 0


