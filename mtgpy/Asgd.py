import torch
from torch.optim.optimizer import Optimizer
from torch.optim import Adam, AdamW

class AvgAdam(Adam):
    def __init__(self, *args, start=200, **kwargs):
        super(AvgAdam, self).__init__(*args, **kwargs)

        self.n_steps = 0
        self.start = start
        self.avg_step = 1
        self.avg_denominator = 1

    def __setstate__(self, state):
        super(AvgAdam, self).__setstate__(state)

    def step(self, closure=None):
        super(AvgAdam, self).step(closure)
        self.n_steps += 1
        if self.n_steps > self.start:
            for group in self.param_groups:
                for p in group['params']:
                    param_state = self.state[p]
                    if 'cache' not in param_state:
                        param_state['cache'] = torch.zeros_like(p.data)
                        param_state['saved'] = torch.zeros_like(p.data)
                    param_state['cache'].add_(self.avg_step * p)
            self.avg_denominator += self.avg_step
            self.avg_step += 1

    def average(self):
        if self.n_steps <= self.start:
            return
        for group in self.param_groups:
            for p in group['params']:
                param_state = self.state[p]
                param_state['saved'].copy_(p.data)
                p.data.copy_(param_state['cache'] / self.avg_denominator)

    def cancel_average(self):
        if self.n_steps <= self.start:
            return
        for group in self.param_groups:
            for p in group['params']:
                param_state = self.state[p]
                p.data.copy_(param_state['saved'])


class AvgAdamW(AdamW):
    def __init__(self, *args, start=200, **kwargs):
        super(AvgAdamW, self).__init__(*args, **kwargs)

        self.n_steps = 0
        self.start = start
        self.avg_step = 1
        self.avg_denominator = 1

    def __setstate__(self, state):
        super(AvgAdamW, self).__setstate__(state)

    def step(self, closure=None):
        super(AvgAdamW, self).step(closure)
        self.n_steps += 1
        if self.n_steps > self.start:
            for group in self.param_groups:
                for p in group['params']:
                    param_state = self.state[p]
                    if 'cache' not in param_state:
                        param_state['cache'] = torch.zeros_like(p.data)
                        param_state['saved'] = torch.zeros_like(p.data)
                    param_state['cache'].add_(self.avg_step * p)
            self.avg_denominator += self.avg_step
            self.avg_step += 1

    def average(self):
        if self.n_steps <= self.start:
            return
        for group in self.param_groups:
            for p in group['params']:
                param_state = self.state[p]
                param_state['saved'].copy_(p.data)
                p.data.copy_(param_state['cache'] / self.avg_denominator)

    def cancel_average(self):
        if self.n_steps <= self.start:
            return
        for group in self.param_groups:
            for p in group['params']:
                param_state = self.state[p]
                p.data.copy_(param_state['saved'])




