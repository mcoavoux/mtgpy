import sys
import os
from collections import defaultdict
import copy
import logging
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import pickle
import random
import time

from hyperpyyaml import load_hyperpyyaml
from speechbrain.core import _convert_to_yaml

from mtgpy.Asgd import AvgAdam, AvgAdamW
from mtgpy.word_encoders import WordEmbedder, Words2Tensors
from mtgpy.sentence_encoders import SentenceEncoderLSTM

import mtgpy.fasttext_embeddings
import mtgpy.discodop_eval as discodop_eval
import mtgpy.corpus_reader as corpus_reader
import mtgpy.tree as T

from mtgpy.features import feature_functions
from mtgpy.state_gap import State


class Parser(nn.Module):
    def __init__(self, args,
                 i2labels, i2tags, i2chars, i2words):
        super(Parser, self).__init__()

        self.discontinuous = not args.projective

        self.args = args
        self.i2labels = i2labels
        self.i2tags =  i2tags
        self.i2chars = i2chars
        self.i2words = i2words

        self.labels2i = {l : i for i,l in enumerate(i2labels)}
        self.tags2i = [{l : i for i,l in enumerate(i2ts)} for i2ts in self.i2tags]
        self.chars2i = {l : i for i,l in enumerate(i2chars)}
        self.words2i = {l : i for i,l in enumerate(i2words)}

        self.words2tensors = Words2Tensors(self.chars2i, self.words2i, pword=args.dropout_word)

        self.num_labels = len(i2labels)
        self.num_tags = [len(i2ts) for i2ts in self.i2tags]
        self.num_words = len(i2words)
        self.num_chars = len(i2chars)

        self.word_embedder = WordEmbedder(args, len(self.i2words), len(self.i2chars), self.words2tensors, self.words2i)

        d_input = self.word_embedder.output_dim

        self.encoder = SentenceEncoderLSTM(args, d_input)
    
        dim_encoder = args.lstm_dim
        if args.enc == "no":
            dim_encoder = self.word_embedder.output_dim

        self.feature_function, num_features = feature_functions[args.feats]

        activation_function = args.fun


        self.structure = nn.Sequential(
                    nn.Dropout(args.dropout),
                    nn.Linear(dim_encoder*num_features, args.dim_hidden),
                    nn.LayerNorm(args.dim_hidden),
                    activation_function(),
                    nn.Linear(args.dim_hidden, 3),
                    nn.LogSoftmax(dim=1))

        self.label = nn.Sequential(
                    nn.Dropout(args.dropout),
                    nn.Linear(dim_encoder*num_features, args.dim_hidden),
                    nn.LayerNorm(args.dim_hidden),
                    activation_function(),
                    nn.Linear(args.dim_hidden, self.num_labels),
                    nn.LogSoftmax(dim=1))

        self.tagger = nn.ModuleList(
                    [nn.Sequential(
                        nn.Dropout(args.dropout),
                        nn.Linear(dim_encoder, args.dim_hidden),
                        nn.LayerNorm(args.dim_hidden),
                        activation_function(),
                        nn.Linear(args.dim_hidden, self.num_tags[0]),
                        nn.LogSoftmax(dim=1))])

        for i in range(1, len(self.num_tags)):
            self.tagger.append(nn.Sequential(
                        nn.Dropout(args.dropout),
                        nn.Linear(dim_encoder, self.num_tags[i]),
                        nn.LogSoftmax(dim=1)))


        self.loss_function = nn.NLLLoss(reduction="sum")
        self.default_values = nn.Parameter(torch.Tensor(4, dim_encoder))

        self.initialize_parameters(args)

    def initialize_parameters(self, args):
        # uniform initialization for embeddings
        torch.nn.init.uniform_(self.default_values, -0.01, 0.01)


    def train_batch(self, batch_sentences, all_embeddings, targets):
        batch_features, batch_tags = targets
        output_l1, output_l2 = all_embeddings

        batch_label_input = []
        batch_label_output = []
        batch_struct_input = []
        batch_struct_output = []

        for sentence_embeddings, features in zip(output_l2, batch_features):
            #batch_features: {"struct": (struct_input, struct_output), "label": (labels_input, labels_output)}
            l_input, l_output = features["label"]
            s_input, s_output = features["struct"]
            
            batch_label_input.append(sentence_embeddings[l_input].view(len(l_input), -1))
            batch_label_output.append(l_output)
            
            batch_struct_input.append(sentence_embeddings[s_input].view(len(s_input), -1))
            batch_struct_output.append(s_output)

        # Struct / label 
        batch_label_input = torch.cat(batch_label_input, dim=0)
        batch_label_output = torch.cat(batch_label_output)

        batch_struct_input = torch.cat(batch_struct_input, dim=0)
        batch_struct_output = torch.cat(batch_struct_output)

        labels_output_tensors = self.label(batch_label_input)
        label_loss = self.loss_function(labels_output_tensors, batch_label_output)
        label_loss /= len(batch_label_input)

        struct_output_tensors = self.structure(batch_struct_input)
        struct_loss = self.loss_function(struct_output_tensors, batch_struct_output)
        struct_loss /= len(batch_struct_input)

        # Tagging / feature prediction
        batch_tagging_input = torch.cat(output_l1, dim=0)
        
        #check sizes
        batch_tagging_targets = [torch.cat([batch_features[i] for batch_features in batch_tags]) for i in range(len(batch_tags[0]))]

        tagging_output = [feature_tagger(batch_tagging_input) for feature_tagger in self.tagger]
        tagging_loss = [torch.sum(self.loss_function(feat_o, bat_feat_tgt)) for feat_o, bat_feat_tgt in zip(tagging_output, batch_tagging_targets)]
        tagging_loss = [feat_loss / len(batch_tagging_targets[0]) for feat_loss in tagging_loss]


        return {"tagging loss": tagging_loss, "struct loss": struct_loss, "label loss": label_loss}


    def forward(self, batch_sentences_raw, targets=None):
        # raw: no SOS / EOS
        batch_sentences = [["<SOS>"] + sent + ["<EOS>"] for sent in batch_sentences_raw]

        lengths = torch.tensor([len(s) for s in batch_sentences])
        all_embeddings = self.word_embedder(batch_sentences)

        if self.args.enc != "no":
            padded_char_based_embeddings = torch.nn.utils.rnn.pad_sequence(all_embeddings, batch_first=True)
            unpacked_l1, unpacked_l2 = self.encoder(padded_char_based_embeddings, lengths)
        else:
            unpacked_l1 = all_embeddings
            unpacked_l2 = all_embeddings

        unpacked_l2 = [torch.cat([self.default_values, l2_sent], dim=0) for l2_sent in unpacked_l2]

        if targets is not None:
            return self.train_batch(batch_sentences, (unpacked_l1, unpacked_l2), targets)

        tokens = [[T.Token(tok, i, [None]) for i, tok in enumerate(sent)] for sent in batch_sentences_raw]

        return self.parse_batch(tokens, (unpacked_l1, unpacked_l2))
        #return self.parse(tokens, (unpacked_l1, unpacked_l2))

    def parse(self, sentences, sentence_embeddings):
        # sentences: list of list of T.Tokens
        output_l1, output_l2 = sentence_embeddings

        lengths = [len(sent) for sent in sentences]
        batch_tagging_input = torch.cat(output_l1, dim=0)
        tag_scores = self.tagger(batch_tagging_input)
        tag_predictions = torch.argmax(tag_scores, dim=1).cpu().split(lengths)
        tag_predictions = [v.numpy() for v in tag_predictions]

        for sent_tags, sent in zip(tag_predictions, sentences):
            for tag, tok in zip(sent_tags, sent):
                tok.set_tag(self.i2tags[tag])

        trees = []
        # make this more parallel / batch
        for sentence, sentence_embedding in zip(sentences, output_l2):
            state = State(sentence, self.discontinuous)
            while not state.is_final():
                next_type = state.next_action_type()
                
                stack, queue, buffer, sent_len = state.get_input()
                input_features = self.feature_function(sentence_embedding.device, stack, queue, buffer, sent_len)
                input_embeddings = sentence_embedding[input_features].view(1, -1)
                if next_type == State.LABEL:
                    output = self.label(input_embeddings).squeeze()
                    state.filter_action(next_type, output)

                    prediction = torch.argmax(output)
                    if prediction == 0:
                        state.nolabel()
                    else:
                        pred_label = self.i2labels[prediction]
                        state.labelX(pred_label)
                else:
                    output = self.structure(input_embeddings).squeeze()
                    state.filter_action(next_type, output)

                    prediction = torch.argmax(output.squeeze())
                    if prediction == State.SHIFT:
                        state.shift()
                    elif prediction == State.COMBINE:
                        state.combine()
                    else:
                        assert(prediction == State.GAP)
                        state.gap()
            trees.append(state.get_tree())
        return trees

    def parse_batch(self, sentences, sentence_embeddings):
        # sentences: list of list of T.Tokens
        output_l1, output_l2 = sentence_embeddings

        lengths = [len(sent) for sent in sentences]
        batch_tagging_input = torch.cat(output_l1, dim=0)
        
        # tag_scores = [feature_type]
        tag_scores = [tag(batch_tagging_input) for tag in self.tagger]
        
        # tag_predictions = [feature, sentence]
        tag_predictions = [torch.argmax(features_scores, dim=1).cpu().split(lengths) for features_scores in tag_scores]
        tag_predictions = [[sent.numpy() for sent in feature_predictions] for feature_predictions in tag_predictions]
        #tag_predictions: [sentence, feature]
        tag_predictions = list(zip(*tag_predictions))

        for sent_tags, sent in zip(tag_predictions, sentences):
            # sent tags:  K list of size len(sentence)
            for i, tok in enumerate(sent):
                for j in range(len(sent_tags)):
                    tok.set_feature(j, self.i2tags[j][sent_tags[j][i]])
            # ~ for tag, tok in zip(sent_tags, sent):
                # ~ tok.set_tag(self.i2tags[tag])

        
        trees = [None for _ in range(len(sentences))]
        states = [State(sentence, self.discontinuous, sent_id=i) for i, sentence in enumerate(sentences)]

        while len(states) > 0:
            next_types = [state.next_action_type() for state in states]
            input_structs = [state.get_input() for state in states]
            input_features = [self.feature_function(output_l2[0].device, stack, queue, buffer, sent_len)
                                for stack, queue, buffer, sent_len in input_structs]

            input_embeddings = [output_l2[state.sent_id][in_feats].view(-1) for state, in_feats in zip(states, input_features)]

            label_states = [states[i] for i, ntype in enumerate(next_types) if ntype == State.LABEL]
            label_inputs = [input_embeddings[i] for i, ntype in enumerate(next_types) if ntype == State.LABEL]

            if len(label_states) > 0:
                label_outputs = self.label(torch.stack(label_inputs))
                for i, state in enumerate(label_states):
                    state.filter_action(State.LABEL, label_outputs[i])
                    prediction = torch.argmax(label_outputs[i])
                    if prediction == 0: 
                        state.nolabel()
                    else:
                        pred_label = self.i2labels[prediction]
                        state.labelX(pred_label)

            struct_states = [states[i] for i, ntype in enumerate(next_types) if ntype == State.STRUCT]
            struct_inputs = [input_embeddings[i] for i, ntype in enumerate(next_types) if ntype == State.STRUCT]

            #print(torch.stack(struct_inputs).shape)
            if len(struct_states) > 0:
                struct_outputs = self.structure(torch.stack(struct_inputs))
                for i, state in enumerate(struct_states):
                    state.filter_action(State.STRUCT, struct_outputs[i])
                    prediction = torch.argmax(struct_outputs[i])
                    if prediction == State.SHIFT:
                        state.shift()
                    elif prediction == State.COMBINE:
                        state.combine()
                    else:
                        assert(prediction == State.GAP)
                        state.gap()

            states = []
            for state in label_states + struct_states:
                if state.is_final():
                    trees[state.sent_id] = state.get_tree()
                else:
                    states.append(state)
        return trees

def predict_corpus(model, corpus, batch_size):
    # corpus: list of list of tokens
    trees = []

    # sort by length for lstm batching
    indices, corpus = zip(*sorted(zip(range(len(corpus)), 
                                            corpus), 
                                            key = lambda x: len(x[1]),
                                            reverse=True))

    with torch.no_grad():
        for i in range(0, len(corpus), batch_size):
            tree_batch = model(corpus[i:i+batch_size])

            for t in tree_batch:
                t.expand_unaries()
                trees.append(t)

    # reorder
    _, trees = zip(*sorted(zip(indices, trees), key = lambda x:x[0]))
    return trees

def assign_pretrained_embeddings(model, ft, dim_ft, freeze):
    print("Assigning pretrained fasttext embeddings")
    embedding = model.word_embedder.word_embeddings
    device = embedding.weight.data.device
    for w, v in ft.items():
        i = model.words2i[w]
        embedding.weight.data[i, :dim_ft] = torch.tensor(v).to(device)

    if freeze:
        print("Freezing word embedding layer")
        embedding.requires_grad_(False)
    

def get_vocabulary(corpus):
    """Extract vocabulary for characters, tokens, non-terminals, POS tags"""
    words = defaultdict(int)

    chars = defaultdict(int)
#    chars["<START>"] += 2
#    chars["<STOP>"] += 2

    # These should be treated as single characters
    chars["-LRB-"] += 2
    chars["-RRB-"] += 2
    chars["#LRB#"] += 2
    chars["#RRB#"] += 2

    tag_size = len(T.get_yield(corpus[0])[0].get_features())
    tag_set = [defaultdict(int) for _ in range(tag_size)]

    label_set = defaultdict(int)
    for tree in corpus:
        tokens = T.get_yield(tree)
        for tok in tokens:
            for char in tok.token:
                chars[char] += 1

            tag_features = tok.get_features()
            for i, tag in enumerate(tag_features):
                tag_set[i][tag] += 1

            words[tok.token] += 1

        constituents = T.get_constituents(tree)
        for label, _ in constituents:
            label_set[label] += 1

    return words, chars, label_set, tag_set

def extract_features(device, labels2i, sentence, feature_function):

    state = State(sentence, True)

    struct_input = []
    struct_output = []

    labels_input = []
    labels_output = []

    while not state.is_final():
        next_type = state.next_action_type()
        gold_action, (stack, queue, buffer, sent_len) = state.oracle()
        input_features = feature_function(device, stack, queue, buffer, sent_len)

        if next_type == State.STRUCT:
            struct_input.append(input_features)
            struct_output.append(State.mapping[gold_action[0]])

        else:
            assert(next_type == State.LABEL)
            labels_input.append(input_features)
            labels_output.append(labels2i[gold_action[1]])


    struct_input = torch.stack(struct_input)
    struct_output = torch.tensor(struct_output, dtype=torch.long, device=device)

    labels_input = torch.stack(labels_input)
    labels_output = torch.tensor(labels_output, dtype=torch.long, device=device)

    #print(labels_output)

    return {"struct": (struct_input, struct_output), "label": (labels_input, labels_output)}


def extract_tags(device, tags2i, sentence):
    # Returns a tensor for tag ids for a single sentence
    #idxes = [tags2i[tok.get_tag()] for tok in sentence]
    #return torch.tensor(idxes, dtype=torch.long, device=device)
    features = [tok.get_features() for tok in sentence]
    idxes = [[tags2i[f][features[i][f]] for i in range(len(features))] for f in range(len(tags2i))]
    return [torch.tensor(f_idxes, dtype=torch.long, device=device) for f_idxes in idxes]

def compute_f(TPs, total_golds, total_preds):
    p, r, f = 0, 0, 0
    if total_preds > 0:
        p = TPs / total_preds
    if total_golds > 0:
        r = TPs / total_golds
    if (p, r) != (0, 0):
        f = 2*p*r / (p+r)
    return p, r, f

def Fscore_corpus(golds, preds):
    TPs = 0
    total_preds = 0
    total_golds = 0
    UTPs = 0
    for gold, pred in zip(golds, preds):
        TPs += len([c for c in gold if c in pred])
        total_golds += len(gold)
        total_preds += len(pred)

        ugold = defaultdict(int)
        for _, span in gold:
            ugold[span] += 1
        upred = defaultdict(int)
        for _, span in pred:
            upred[span] += 1
        for span in upred:
            UTPs += min(upred[span], ugold[span])

    p, r, f = compute_f(TPs, total_golds, total_preds)

    up, ur, uf = compute_f(UTPs, total_golds, total_preds)
    return p*100, r*100, f*100, up*100, ur*100, uf*100


def prepare_corpus(corpus):
    sentences = [T.get_yield(corpus[i]) for i in range(len(corpus))]
    raw_sentences = [[tok.token for tok in sentence] for sentence in sentences]
    return sentences, raw_sentences


def eval_tagging(gold, pred):
    # Returns accuracy for tag predictions
    acc = [0] * gold[0][0].len_feature()
    tot = 0
    assert(len(gold) == len(pred))
    for sent_g, sent_p in zip(gold, pred):
        assert(len(sent_g) == len(sent_p))
        for tok_g, tok_p in zip(sent_g, sent_p):
            for i in range(tok_g.len_feature()):
                if tok_g.get_tag(i) == tok_p.get_tag(i):
                    acc[i] += 1
        tot += len(sent_g)
    return [accuracy * 100 / tot for accuracy in acc]

def train_epoch(device, epoch, model, optimizer, args, logger, train_raw_sentences, features, tag_features, idxs, scheduling_dict, steps_per_epoch, check_callback):

    tag_losses = [0 for _ in tag_features[0]]
    struct_loss = 0
    label_loss = 0

    model.train()

    if args.shuffle_train:
        print("Shuffling")
        random.shuffle(idxs)

    #n_batches = len(idxs)//batch_size
    #check_id = n_batches // args.check
    #num_check = 0
    i = 0
    j = 0
    
    while i < len(idxs):
    #for j, i in enumerate(range(0, len(idxs), batch_size)):
        batch_size = args.train_batchsize
        # ~ if args.train_batchsize_max is not None:
            # ~ #batch_size = np.random.randint(args.train_batchsize, args.train_batchsize_max + 1)
            # ~ if j % 2 == 0:
                # ~ batch_size = args.train_batchsize_max

        batch_sentences = [train_raw_sentences[idx] for idx in idxs[i:i+batch_size]]
        batch_features = [features[idx] for idx in idxs[i:i+batch_size]]
        batch_tags = [tag_features[idx] for idx in idxs[i:i+batch_size]]



#        print(batch_features[0])
#        print(batch_tags[0])
        batch_features = [ {k: (v[0].to(device), v[1].to(device)) for k,v in feats.items()} for feats in batch_features]
        batch_tags = [[feats.to(device) for feats in features] for features in batch_tags]

        batch_sentences, batch_features, batch_tags = zip(*sorted(zip(batch_sentences, batch_features, batch_tags), 
                                                           key = lambda x: len(x[0]), reverse=True))

        optimizer.zero_grad()
        losses = model(batch_sentences, targets=(batch_features, batch_tags))

        batch_total_loss = losses["struct loss"] + losses["label loss"] + sum(losses["tagging loss"])
        batch_total_loss.backward()

        for k in range(len(tag_losses)):
            tag_losses[k] += losses["tagging loss"][k].item()
        struct_loss += losses["struct loss"].item()
        label_loss += losses["label loss"].item()

        optimizer.step()

        if j % 20 == 0 and args.v > 0:
            logger.info(f"Epoch {epoch} Training batch {j}/~{steps_per_epoch} sent {i} to {i+batch_size} lr:{list(optimizer.param_groups)[0]['lr']:.4e}")

        scheduling_dict["total_steps"] += 1
        scheduling_dict["scheduler"].step()
        #print(scheduling_dict["scheduler"].get_last_lr())
        # ~ if scheduling_dict["total_steps"] <= scheduling_dict["warmup_steps"]:
            # ~ set_lr(optimizer, scheduling_dict["total_steps"] * scheduling_dict["warmup_coeff"])

        # ~ if (j + 1) % check_id == 0 and num_check < args.check - 1 and epoch >= 8:
            # ~ num_check += 1
            # ~ check_callback(epoch, num_check, False)
        j += 1
        i += batch_size

    return {"tag loss": tag_losses, "struct loss": struct_loss, "label loss": label_loss}


# ~ def set_lr(optimizer, new_lr):
    # ~ for param_group in optimizer.param_groups:
        # ~ param_group['lr'] = new_lr


def eval_on_corpus(args, model, sentences, raw_sentences, corpus):
    pred_trees = predict_corpus(model, raw_sentences, args.eval_batchsize)

    sentence_pred_tokens = [T.get_yield(tree) for tree in pred_trees]

    tageval = eval_tagging(sentences, sentence_pred_tokens)

    outfile = f"{args.model}/tmp_{corpus}.discbracket"
    with open(outfile, "w", encoding="utf8") as fstream:
        for tree in pred_trees:
            fstream.write("{}\n".format(str(tree)))

    if corpus == "dev":
        goldfile = args.dev
    else:
        goldfile = f"{args.model}/tmp_sample_train_gold.discbracket"

    discop, discor, discodop_f = discodop_eval.call_eval(goldfile, outfile, args.eval_config, disconly=False)
    disco2p, disco2r, discodop2_f = discodop_eval.call_eval(goldfile, outfile, args.eval_config, disconly=True)

    return {"p": discop, "r": discor, "f": discodop_f,
            "dp": disco2p, "dr": disco2r, "df": discodop2_f, 
            "tag": "/".join([str(round(tagacc, 2)) for tagacc in tageval])}


def merge_tags(corpus, tags, args):
    assert len(corpus) == len(tags)
    
    feat_list = args.multitask.split()
    
    for tree, sentence_tags in zip(corpus, tags):
        tokens = T.get_yield(tree)
        ttokens, ttags = sentence_tags
        
        assert len(tokens) == len(ttokens)
        
        for i in range(len(tokens)):
            # ~ if tokens[i].token != supertags[i][0] :
                # ~ print(tokens[i].token, supertags[i][0])
            assert tokens[i].token == ttokens[i]
            for feat in feat_list:
                tokens[i].add_feature(f"{feat}={ttags[i][feat]}")


def load_corpora(args, logger):
    logger.info("Loading corpora...")
    if args.fmt == "discbracket":
        train_corpus = corpus_reader.read_discbracket_corpus(args.train)
        dev_corpus = corpus_reader.read_discbracket_corpus(args.dev)
    elif args.fmt == "bracket":
        train_corpus = corpus_reader.read_bracket_corpus(args.train)
        dev_corpus = corpus_reader.read_bracket_corpus(args.dev)

    for tree in train_corpus:
        tree.merge_unaries()
    
    if args.multitask is not None:
        logger.info("Loading tags from external files")
        
        header, train_tags = corpus_reader.read_tags(args.train.replace(args.fmt, "tags"))
        header, dev_tags   = corpus_reader.read_tags(args.dev.replace(args.fmt, "tags"))
        
        merge_tags(train_corpus, train_tags, args)
        merge_tags(dev_corpus, dev_tags, args)

    return train_corpus, dev_corpus

def get_voc_dicts(words, vocabulary, label_set, tag_set):
    i2chars = ["<PAD>", "<UNK>", "<START>", "<STOP>", "<SOS>", "<EOS>"] + sorted(vocabulary, key = lambda x: vocabulary[x], reverse=True)
    #chars2i = {k:i for i, k in enumerate(i2chars)}

    i2labels = ["nolabel"] + sorted(label_set)
    labels2i = {k: i for i, k in enumerate(i2labels)}

    
    i2tags = [sorted(feature_set) for feature_set in tag_set]
    tags2i = [{k:i for i, k in enumerate(i2ts)} for i2ts in i2tags]

    i2words = ["<PAD>", "<UNK>", "<SOS>", "<EOS>"] + sorted(words, key=lambda x: words[x], reverse=True)
    #words2i = {k:i for i, k in enumerate(i2words)}
    return i2chars, i2labels, labels2i, i2tags, tags2i, i2words

def check_dev(device, epoch, check_id, optimizer, model, args, dev_sentences, dev_raw_sentences, sample_train, sample_train_raw, scheduling_dict, average):
    optimizer.zero_grad()
    model.eval()

    if args.optim.startswith("avg") and average:
        optimizer.average()

    dev_results = eval_on_corpus(args, model, dev_sentences, dev_raw_sentences, "dev")
    train_results = eval_on_corpus(args, model, sample_train, sample_train_raw, "train")
    discodop_f = dev_results["f"]
    discodop_df = dev_results["df"]

    print(f"Epoch {epoch}.{check_id} lr:{optimizer.param_groups[0]['lr']:.4e}",
         "dev:",   " ".join([f"{k}={v}" for k, v in sorted(dev_results.items())]),
         "train:", " ".join([f"{k}={v}" for k, v in sorted(train_results.items())]),
#              f" dev tag:{dtag:.2f} f:{discodop_f} p:{discop} r:{discor} disco f:{discodop2_f} p:{disco2p} r:{disco2r}", 
#              f" train tag:{dtag:.2f} f:{discodop_f} p:{discop} r:{discor} disco f:{discodop2_f} p:{disco2p} r:{disco2r}", 
          f"best: f={scheduling_dict['best_dev_f']} df={scheduling_dict['best_dev_df']} ep {scheduling_dict['best_epoch']} avg={int(average)}",
          flush=True)

    #if discodop_f + discodop_df > scheduling_dict['best_dev_f'] + scheduling_dict['best_dev_df']:
    if discodop_f > scheduling_dict['best_dev_f']:
        scheduling_dict["best_epoch"] = epoch
        scheduling_dict['best_dev_f'] = discodop_f
        scheduling_dict['best_dev_df'] = discodop_df
        model.cpu()
        model.words2tensors.to(torch.device("cpu"))
        torch.save(model.state_dict(), "{}/model".format(args.model))
        with open(f"{args.model}/best_dev_score", "w", encoding="utf8") as ff:
            ff.write(str(scheduling_dict['best_dev_f']))
        model.to(device)
        model.words2tensors.to(device)

    if args.optim.startswith("avg") and average:
        optimizer.cancel_average()

    model.train()


def do_training(args, device, logger, model, optimizer, train_corpus, dev_corpus, labels2i, tags2i, num_epochs, steps_per_epoch):

    logger.info("Constructing training examples...")
    train_sentences, train_raw_sentences = prepare_corpus(train_corpus)

    dev_sentences, dev_raw_sentences = prepare_corpus(dev_corpus)

    # just 500 training sentences to monitor learning
    sample_indexes = sorted(np.random.choice(len(train_sentences), min(500, len(train_sentences)), replace=False))
    sample_train = [train_sentences[i] for i in sample_indexes]
    sample_train_raw = [train_raw_sentences[i] for i in sample_indexes]

    sample_trainfile = f"{args.model}/tmp_sample_train_gold.discbracket"
    with open(sample_trainfile, "w", encoding="utf8") as fstream:
        for i in sample_indexes:
            tree = train_corpus[i]
            tree.expand_unaries()
            fstream.write("{}\n".format(str(tree)))
            tree.merge_unaries()


    feature_function, _ = feature_functions[args.feats]
    features = [extract_features(torch.device("cpu"), labels2i, sentence, feature_function) for sentence in train_sentences]
    tag_features = [extract_tags(torch.device("cpu"), tags2i, sentence) for sentence in train_sentences]

    idxs = list(range(len(train_sentences)))
    #num_tokens = sum([len(sentence) for sentence in train_sentences])
    num_tokens = [len(sentence) for sentence in train_sentences]
    idxs.sort(key = lambda i: num_tokens[i], reverse=True)
    
    #print([num_tokens[i] for i in idxs[:20]])
    #random.shuffle(idxs)

    logger.info("Starting training")

    #print(f"warmup steps: {steps_per_epoch}")
    scheduling_dict = {"total_steps": 0,
                       "scheduler": torch.optim.lr_scheduler.SequentialLR(optimizer,
                                [torch.optim.lr_scheduler.LinearLR(
                                    optimizer, start_factor=0.01, end_factor=1,
                                    total_iters=steps_per_epoch),
                                 torch.optim.lr_scheduler.LinearLR(
                                            optimizer, start_factor=1, end_factor=args.lr_scheduler_factor,
                                            total_iters=steps_per_epoch *(args.epochs -1))],
                                 milestones=[steps_per_epoch]),
                       "best_epoch": 0,
                       "best_dev_f": 0,
                       "best_dev_df": 0}

    #set_lr(optimizer, scheduling_dict["warmup_coeff"])


    check_callback = lambda epoch, check_id, avg: check_dev(device, epoch, check_id, optimizer, model, args, 
                                                    dev_sentences, dev_raw_sentences, 
                                                    sample_train, sample_train_raw, scheduling_dict, avg)

    # TODO: change criterion to stop training
    for epoch in range(1, num_epochs+1):

        times =  [time.time()]
        epoch_dict = train_epoch(device, epoch, model, optimizer, args, logger, train_raw_sentences, features, tag_features, idxs, scheduling_dict, steps_per_epoch, check_callback)
        times.append(time.time())

        tag_losses = epoch_dict["tag loss"]
        tag_losses_str = "/".join([f"{tloss:.4f}" for tloss in tag_losses])
        struct_loss = epoch_dict["struct loss"]
        label_loss = epoch_dict["label loss"]

        # ~ if epoch < 10 and epoch % 2 == 1:
            # ~ train_time = round(times[1] -times[0])
            # ~ print(f"Epoch {epoch} tag:{tag_losses_str} str:{struct_loss:.4f} lab:{label_loss:.4f} train time {train_time}s")
            # ~ continue


        check_callback(epoch, args.check, False)
        times.append(time.time())
        

        if args.optim.startswith("avg") and epoch >= args.start_averaging:
            check_callback(epoch, args.check, True)
            times.append(time.time())

        train_time = round(times[1] -times[0])
        eval_time1 = round(times[2] - times[1])
        eval_time2 = round(times[3] - times[2]) if len(times) > 3 else 0
        
        print(f"Epoch {epoch} tag:{tag_losses_str} str:{struct_loss:.4f} lab:{label_loss:.4f} train time {train_time}s eval time: {eval_time1}s {eval_time2}s")

        # ~ if scheduling_dict["total_steps"] > scheduling_dict["warmup_steps"]:
            # ~ scheduling_dict["scheduler"].step(scheduling_dict["best_dev_f"])
            # ~ if epoch - scheduling_dict["best_epoch"] > (scheduling_dict["patience"] + 1) * 3:
                # ~ print("Finishing training: no improvement on dev.")
                # ~ break

    if args.optim.startswith("avg"):
        optimizer.average()


def main_train(args, logger, device):
    train_corpus, dev_corpus = load_corpora(args, logger)

    if args.fast_text is not None:
        logger.info("Loading fast text vectors")
        ft, dim_ft = mtgpy.fasttext_embeddings.load_vectors(args.fast_text)
        logger.info("Loading fast text vectors: done")
    else:
        ft = None
        dim_ft = None

    logger.info("Vocabulary extraction...")
    words, vocabulary, label_set, tag_set = get_vocabulary(train_corpus)

    i2chars, i2labels, labels2i, i2tags, tags2i, i2words = get_voc_dicts(words, vocabulary, label_set, tag_set)

    if ft is not None:
        voc_set = set(i2words)
        for token in ft:
            if token not in voc_set:
                i2words.append(token)

    logger.info("Model initialization...")
    model = Parser(args, i2labels, i2tags, i2chars, i2words)
    
    if ft is not None:
        assign_pretrained_embeddings(model, ft, dim_ft, freeze=args.freeze_ft)

    with open(f"{args.model}/data", "wb") as fp:
        pickle.dump([i2chars, i2labels, i2tags, i2words, args], fp)

    model.to(device)
    model.words2tensors.to(device)

    if args.S is not None:
        train_corpus = train_corpus[:args.S]
        dev_corpus = dev_corpus[:args.S]

    print("Training sentences: {}".format(len(train_corpus)))
    print("Dev set sentences: {}".format(len(dev_corpus)))


    num_parameters = 0
    num_parameters_embeddings = 0
    num_parameters_bert = 0
    for name, p in model.named_parameters():
        #print("parameter '{}', {}".format(name, p.numel()))
        if "bert" in name:
            num_parameters_bert += p.numel()
        elif "embedding" in name:
            num_parameters_embeddings += p.numel()
        else:
            num_parameters += p.numel()
    total = num_parameters + num_parameters_embeddings + num_parameters_bert
    print(f"Total number of parameters: {total/10**6:.3f}")
    print(f"Bert parameters: {num_parameters_bert/10**6:.3f}")
    print(f"Embedding parameters: {num_parameters_embeddings/10**6:.3f}")
    print(f"Non embedding parameters: {num_parameters/10**6:.3f}")

    parameters = [pp for pp in model.parameters() if pp.requires_grad]


    assert(args.optim in "adam adamw avg_adam avg_adamw sgd".split())

    steps_per_epoch = int(len(train_corpus) / args.train_batchsize)
    # ~ if args.train_batchsize_max is not None:
        # ~ steps_per_epoch = int(len(train_corpus) * 2 / (args.train_batchsize + args.train_batchsize_max))
        
    start = steps_per_epoch * args.start_averaging # start averaging after 20 epochs
    if args.optim == "adam":
        optimizer = optim.Adam(parameters, lr=args.lr)
    elif args.optim == "adamw":
        optimizer = optim.AdamW(parameters, lr=args.lr)
    elif args.optim == "avg_adam":
        optimizer = AvgAdam(parameters, lr=args.lr, start=start)
    elif args.optim == "avg_adamw":
        optimizer = AvgAdamW(parameters, lr=args.lr, start=start)
    elif args.optim == "sgd":
        optimizer = optim.SGD(parameters, lr=args.lr)

    do_training(args, device, logger, model, optimizer, train_corpus, dev_corpus, labels2i, tags2i, args.epochs, steps_per_epoch)

def read_raw_corpus(filename):
    sentences = []
    with open(filename, encoding="utf8") as f:
        for line in f:
            # For negra
            line = line.replace("(", "#LRB#").replace(")", "#RRB#")
            line = line.strip().split()
            if len(line) > 0:
                sentences.append(line)
    return sentences

def load_data(modeldir):
    with open(f"{modeldir}/data", "rb") as fp:
        return pickle.load(fp)

def main_eval(args, logger, device):

    i2chars, i2labels, i2tags, i2words, args_train = load_data(f"{args.model}")

    model = Parser(args_train, i2labels, i2tags, i2chars, i2words)
    
    state_dict = torch.load("{}/model".format(args.model))
    model.load_state_dict(state_dict)
    model.to(device)
    model.words2tensors.to(device)
    model.eval()
    logger.info("Model loaded")
    

    sentences = read_raw_corpus(args.test_tokens)
    #sentence_toks = [[T.Token(token, i, [None]) for i, token in enumerate(sentence)] for sentence in sentences]

    trees = predict_corpus(model, sentences, args.eval_batchsize)

    if args.output is None:
        for tree in trees:
            print(tree)
    else:
        with open(args.output, "w", encoding="utf8") as f:
            for tree in trees:
                f.write("{}\n".format(str(tree)))
        corpus_reader.write_conll_corpus(trees, args.output.replace("discbracket", "conll"))
        with open(f"{args.output}.tags", "w", encoding="utf8") as f:
            sentences = [T.get_yield(t) for t in trees]
            for sentence in sentences:
                for token in sentence:
                    token_str = "\t".join([token.token] + token.features)
                    f.write(f"{token_str}\n")
                f.write("\n")

        if args.gold is not None:
            p, r, f = discodop_eval.call_eval(args.gold, args.output, args.eval_config, disconly=False)
            dp, dr, df = discodop_eval.call_eval(args.gold, args.output, args.eval_config, disconly=True)

            print(f"precision={p}")
            print(f"recall={r}")
            print(f"fscore={f}")
            print(f"disc-precision={dp}")
            print(f"disc-recall={dr}")
            print(f"disc-fscore={df}")

            pred_tokens = [T.get_yield(tree) for tree in trees]
            #gold_tokens = [T.get_yield(tree) for tree in corpus_reader.read_discbracket_corpus(args.gold)]
            gold_trees = corpus_reader.read_discbracket_corpus(args.gold)
            gold_tokens = [T.get_yield(tree) for tree in gold_trees]

            if args.multitask is not None:
                logger.info("Loading tags")
                header, test_tags = corpus_reader.read_tags(args.gold.replace(args.fmt, "tags"))
                
                merge_tags(gold_trees, test_tags, args)

            tag_eval = eval_tagging(gold_tokens, pred_tokens)
            tag_eval_str = "/".join([str(round(tagacc, 2)) for tagacc in tag_eval])

            print(f"tagging={tag_eval_str}")


def main(args, logger, device):
    """
        mtgpy: a transition-based constituency parser
    """
    if args.mode == "train":
        try:
            main_train(args, logger, device)
        except KeyboardInterrupt:
            print("Training interrupted, exiting")
            return

    if args.test_tokens is not None and args.gold is not None:
        main_eval(args, logger, device)
    else:
        print("No test set provided")

def set_seed(args):
    SEED = 0
    if args.mode == "train":
        SEED = args.seed
    random.seed(SEED)
    np.random.seed(SEED)
    torch.manual_seed(SEED)




def cli():
    sys.setrecursionlimit(1500)

    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG, 
                format='%(asctime)s-%(relativeCreated)d-%(levelname)s:%(message)s')
    import argparse

    usage = main.__doc__

    parser = argparse.ArgumentParser(description = usage, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
    parser.add_argument("mode", help='train: training, eval: test')
    parser.add_argument("config", help="Path to yaml config file")

    args, overrides = parser.parse_known_args()
    overrides = _convert_to_yaml(overrides)
    
    with open(args.config) as fin:
        hparams = load_hyperpyyaml(fin, overrides)

    for k, v in vars(args).items():
        hparams[k] = v
    
    args = argparse.Namespace(**hparams)

    if args.fast_text is not None:
        args.dim_word_embeddings  = 300

    for k, v in vars(args).items():
        print(k, v)
    
    torch.set_num_threads(args.threads)
    logger = logging.getLogger()
    logger.info("Mode={}".format(args.mode))

    if args.mode == "train":
        os.makedirs(args.model, exist_ok = True)

    
    if not torch.cuda.is_available():
        args.device = "cpu"
    device = torch.device(args.device)
    
    logger.info(f"Using device: {device}")
    set_seed(args)
    main(args, logger, device)


if __name__ == "__main__":
    cli()
