if [ "$#" -le 2 ]
then
    echo "illegal number of parameters"
    echo Usage:
    echo    "bash expe_main.sh (negra|dptb|tiger_spmrl) modelname *args"
    exit 1
fi

source /home/getalp/coavouxm/anaconda3/bin/activate discoparset
mod=$2
mkdir -p ${mod}

git rev-parse HEAD > ${mod}/git_rev
conda list --explicit > ${mod}/conda-requirements.txt
pip freeze > ${mod}/requirements.txt

corpus=$1

tr=~/data/multilingual_disco_data/data/${corpus}/train.discbracket
dev=~/data/multilingual_disco_data/data/${corpus}/dev.discbracket

dtok=~/data/multilingual_disco_data/data/${corpus}/dev.tokens
ttok=~/data/multilingual_disco_data/data/${corpus}/test.tokens

tgold=~/data/multilingual_disco_data/data/${corpus}/test.discbracket


shift
shift


args="${tr} ${dev} $@"


echo python ../src/mtg.py train ${mod} ${args}                                                          > ${mod}/commandline

python ../src/mtg.py train ${mod} ${args}                                                          > ${mod}/log.txt 2> ${mod}/err.txt &&
python ../src/mtg.py eval ${mod} ${dtok} ${mod}/dev_pred.discbracket  --gpu 0 -t 2 --gold ${dev}   > ${mod}/eval_dev &&
python ../src/mtg.py eval ${mod} ${ttok} ${mod}/test_pred.discbracket --gpu 0 -t 2 --gold ${tgold} > ${mod}/eval_test

