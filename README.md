# Installation

```bash
git clone https://gitlab.com/mcoavoux/mtgpy
cd mtgpy
pip install .
```

If you use the parser in training mode, you have also to install [disco-dop](https://github.com/andreasvc/disco-dop).

# Parsing with pretrained models

```bash
    conda activate mtg
    
    python src/mtg.py eval <path to model> <path to input data>  <path to output> [--gpu 0]
```

The input data should contain 1 tokenized sentence per line.
Bracket characters `(` `)` should be replaced by `-LRB-` and `-RRB-`.